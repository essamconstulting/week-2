package au.com.essam.spinners.model;

import javafx.scene.paint.*;


public class Spinner {
    private int segmentCount;
    private Color player1Color;
    private Color player2Color;

    public Spinner() {
        this(2, new Color(0., 0., 0., 1.), new Color(1., 1., 1., 1.));
    }

    public Spinner(int segmentCount, Color player1Color, Color player2Color) {
        this.segmentCount = segmentCount;
        this.player1Color = player1Color;
        this.player2Color = player2Color;
    }

    public int getSegmentCount() {
        return segmentCount;
    }

    public Color getPlayer1Color() {
        return player1Color;
    }

    public Color getPlayer2Color() {
        return player2Color;
    }
}
