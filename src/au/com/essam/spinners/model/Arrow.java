package au.com.essam.spinners.model;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

public class Arrow extends Observable {
    private static final double ANGLE_MULTIPLIER = 0.05;

    private double time;
    private double deltaTime;

    private double speed;
    private double angle;

    private double mu;
    private Timer frictionTimer;
    private TimerTask friction;

    private boolean isSpinning;

    public Arrow(double coefficientOfFriction) {
        this.time = System.currentTimeMillis()/1000.;
        this.deltaTime = (System.currentTimeMillis()/1000. - time);
        this.mu = coefficientOfFriction;

        this.speed = 0.;
        this.angle = 0.;

        this.frictionTimer = new Timer();
        this.friction = new TimerTask() {
            @Override
            public void run() {
                if (isSpinning) {
                    deltaTime = (System.currentTimeMillis() / 1000. - time);
                    time = System.currentTimeMillis() / 1000.;
                } else {
                    deltaTime = 0.;
                    time = System.currentTimeMillis() / 1000.;
                    isSpinning = true;
                }

//                System.out.println("Speed:"+speed+", Angle:"+Math.toDegrees(angle%(2*Math.PI)));

                if (speed < 0.1) {
                    frictionTimer.purge();
                    isSpinning = false;

                    return;
                }

                angle += deltaTime*speed*ANGLE_MULTIPLIER;
                speed -= deltaTime*speed*mu;

                setChanged();
                notifyObservers();
            }
        };
    }

    public void spin() {
        speed += 100.*Math.random();
        if (!isSpinning) {
            frictionTimer.scheduleAtFixedRate(friction, 0, 33);
        }
    }

    public double getRotationInDegrees() {
        return Math.toDegrees(angle);
    }

    public Timer getFrictionTimer() {
        return frictionTimer;
    }
}
