package au.com.essam.spinners;

import au.com.essam.spinners.model.Arrow;
import au.com.essam.spinners.model.Spinner;
import au.com.essam.spinners.view.SpinnerControl;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class SpinnerMain extends Application{

    public static Stage primaryStage;
    private SpinnerControl spinnerNode;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        SpinnerMain.primaryStage = primaryStage;

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                spinnerNode.getArrow().getFrictionTimer().cancel();
                primaryStage.close();
            }
        });

        Color player1Color = new Color(0.8, 0.1, 0.1, 1.0);
        Color player2Color = new Color(0.1, 0.1, 0.8, 1.0);
        spinnerNode = new SpinnerControl(new Spinner(8, player1Color, player2Color));
        Button button = new Button("Spin");
        button.setOnAction(event -> spinnerNode.spin());
        spinnerNode.getChildren().add(button);

        spinnerNode.setMinSize(600., 600.);
        spinnerNode.setPrefSize(600., 600.);
        spinnerNode.setMaxSize(600., 600.);

        Scene scene = new Scene(spinnerNode, 600., 600.);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
