package au.com.essam.spinners.view;

import au.com.essam.spinners.model.Arrow;
import au.com.essam.spinners.model.Spinner;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.transform.Rotate;

import java.util.Observable;
import java.util.Observer;

public class SpinnerControl extends AnchorPane implements Observer {

    private Spinner spinner;
    private Arrow arrow;

    public SpinnerControl(Spinner spinner) {
        this.spinner = spinner;
        this.arrow = new Arrow(0.8);
        this.arrow.addObserver(this);

        Platform.runLater(() -> {
            initialize();

            this.heightProperty().addListener(
                (observable, oldValue, newValue) -> {
                    initialize();
                }
            );
            this.widthProperty().addListener(
                (observable, oldValue, newValue) -> {
                    initialize();
                }
            );
        });

    }

    private void initialize() {
        double width = getWidth();
        double height = getHeight();
        double radius = -1;

        double centerX = width/2.;
        double centerY = height/2.;

        if (width > height) {
            radius = height/2;
        } else if (height > width) {
            radius = width/2;
        } else {
            radius = width/2;
        }

        Circle circle = new Circle(centerX, centerY, radius);
        circle.setStroke(new Color(0., 0., 0., 1.0));

        getChildren().add(circle);

        double displacement = 2*Math.PI/spinner.getSegmentCount();
        for (int i = 0; i < spinner.getSegmentCount(); i++) {
            double startAngle = displacement*i;
            double endAngle = displacement*(i + 1);
            Color color;

            if (i % 2 == 0) {
                color = spinner.getPlayer1Color();
            } else {
                color = spinner.getPlayer2Color();
            }
            getChildren().add(drawSector(startAngle, endAngle, circle, color));
        }

        Line line = new Line(centerX, centerY, centerX, 5);
        line.setStroke(Color.WHITE);
        line.setId("arrow");

        getChildren().add(line);
    }

    private Node drawSector(double startAngle, double endAngle, Circle circle, Color color) {
        double centerX = circle.getCenterX();
        double centerY = circle.getCenterY();
        double radius = circle.getRadius() - 3.;

        Path path = new Path();
        path.setStroke(color);
        path.setFill(color);
        path.setFillRule(FillRule.EVEN_ODD);
        path.setLayoutX(centerX);
        path.setLayoutY(centerY);

        MoveTo centerMove1 = new MoveTo(0, 0);

        double startX = radius*Math.cos(startAngle);
        double startY = radius*Math.sin(startAngle);
        LineTo firstLine = new LineTo(startX, startY);

        double endX = radius*Math.cos(endAngle);
        double endY = radius*Math.sin(endAngle);
        ArcTo arcTo = new ArcTo();
        arcTo.setRadiusX(radius);
        arcTo.setRadiusY(radius);
        arcTo.setX(endX);
        arcTo.setY(endY);
        arcTo.setSweepFlag(true);

        LineTo secondLine = new LineTo(0, 0);

        path.getElements().addAll(centerMove1, firstLine, arcTo, secondLine);

        return path;
    }

    public void spin() {
        arrow.spin();
    }

    public Arrow getArrow() {
        return arrow;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Arrow) {
            Line line = null;
            for (Node child : getChildren()) {
                if ("arrow".equals(child.getId())) {
                    line = (Line) child;
                }
            }

            if (line != null) {
                line.getTransforms().clear();
                line.getTransforms().add(new Rotate(arrow.getRotationInDegrees(), getWidth()/2., getHeight()/2.));
            }
        }
    }
}
