package au.com.essam.tablets;

public class Tablet {
    private String brandName;
    private double price;

    public Tablet(String brandName, double price) {
        this.brandName = brandName;
        this.price = price;
    }

    public void turnOn() {
        System.out.println("Welcome... "+brandName);
    }

}
