package au.com.essam.tablets;

public class TabletMain {

    public static void main(String[] args) {
        Tablet samsungTablet = new Tablet("Samsung", 100.);
        Tablet appleTablet = new Tablet("Apple", Double.POSITIVE_INFINITY);
        samsungTablet.turnOn();
        appleTablet.turnOn();
    }

}
